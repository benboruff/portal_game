# Portal Game

**A sample elixir app**

From [howistart elixir](https://howistart.org/posts/elixir/1/index.html)

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `portal_game` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:portal_game, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/portal_game/api-reference.html](https://hexdocs.pm/portal_game/api-reference.html).
