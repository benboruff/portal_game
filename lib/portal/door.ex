defmodule Portal.Door do
  use Agent

  @moduledoc """
  Using agents, portal doors maintain state.
  """

  @doc """
  Starts a door with the given `color`.

  The color is given as a name so we can identify
  the door by color name instead of using a PID.

  ## Example

      iex> {:ok, pid} = Portal.Door.start_link(:yellow) # {:ok, #<PID<something.something.something>}
      iex> is_pid(pid)
      true

  """
  def start_link(color) do
    Agent.start_link(fn -> [] end, name: color)
  end

  @doc """
  Get the data currently in the `door`.

  ## Example

      iex> Portal.Door.start_link(:rust)
      iex> Portal.Door.push(:rust, 1)
      iex> Portal.Door.get(:rust)
      [1]

  """
  def get(door) do
    Agent.get(door, fn list -> list end)
  end

  @doc """
  Pushes `value` into the door.

  ## Example

      iex> Portal.Door.start_link(:pink)
      iex> Portal.Door.push(:pink, 1)
      :ok

  """
  def push(door, value) do
    Agent.update(door, fn list -> [value | list] end)
  end

  @doc """
  Pops a value from the `door`.

  Returns `{:ok, value}` if there is a value
  or `:error` if the portal is empty.

  ## Example

      iex> Portal.Door.start_link(:black)
      iex> Portal.Door.pop(:black)
      :error

      iex> Portal.Door.start_link(:white)
      iex> Portal.Door.push(:white, 1)
      iex> Portal.Door.pop(:white)
      {:ok, 1}

  """
  def pop(door) do
    Agent.get_and_update(door, fn
      [] -> {:error, []}
      [head | tail] -> {{:ok, head}, tail}
    end)
  end

  @doc """
  Resets the door to []

  ## Example

      iex> Portal.Door.start_link(:grey)
      iex> Portal.Door.push(:grey, 1)
      iex> Portal.Door.reset(:grey)
      iex> Portal.Door.get(:grey)
      []

  """
  def reset(door) do
    Agent.update(door, fn _ -> [] end)
  end
end
