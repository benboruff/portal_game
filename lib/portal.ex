defmodule Portal do
  defstruct [:left, :right]

  @moduledoc """
  The Portal Game demonstrates some
  of `elixir`'s best features.

    * pattern-matching
    * fault-tolerance using supervision trees
    * distributed processes

  """

  @doc """
  Starts transferring `data` from `left` to `right`.

  ## Example

      iex> Portal.shoot(:lime) # the left door
      iex> Portal.shoot(:peach) # the right door
      iex> Portal.transfer(:lime, :peach, [1,2,3])
      #Portal< :lime <=> :peach [1, 2, 3] <=> []>


  """
  def transfer(left, right, data) do
    # First add all data to the portal on the left
    for item <- data do
      Portal.Door.push(left, item)
    end

    # Returns a portal struct we will use next
    %Portal{left: left, right: right}
  end

  defp push_to_direction(portal, {to, from}) do
    # See if we can pop data from the `from`-side. If so, push the
    # popped data to the `to`-side. Otherwise, do nothing.
    case Portal.Door.pop(Map.get(portal, from)) do
      :error -> :ok
      {:ok, head} -> Portal.Door.push(Map.get(portal, to), head)
    end

    # Let's return the portal itself
    portal
  end

  @doc """
  Pushes data to the right in the given `portal`.

  ## Example

      iex> Portal.shoot(:tan) # the left door
      iex> Portal.shoot(:brown) # the right door
      iex> portal = Portal.transfer(:tan, :brown, [1,2,3])
      iex> Portal.push_right(portal)
      #Portal< :tan <=> :brown [1, 2] <=> [3]>

  """
  def push_right(portal) do
    push_to_direction(portal, {:right, :left})
  end

  @doc """
  Pushes data to the left in the given `portal`.

  ## Example

      iex> Portal.shoot(:blaze_red) # the left door
      iex> Portal.shoot(:light_blue) # the right door
      iex> portal2 = Portal.transfer(:blaze_red, :light_blue, [1,2,3])
      iex> Portal.push_right(portal2)
      iex> Portal.push_left(portal2)
      #Portal< :blaze_red <=> :light_blue  [1, 2, 3] <=> []>

  """
  def push_left(portal) do
    push_to_direction(portal, {:left, :right})
  end

  @doc """
  Shoots a new door with the given color.

  ## Example

      iex> {:ok, pid} = Portal.shoot(:crimson)
      iex> is_pid(pid)
      true

  """
  def shoot(color) do
    DynamicSupervisor.start_child(Portal.DoorSupervisor, {Portal.Door, color})
  end
end

defimpl Inspect, for: Portal do
  def inspect(%Portal{left: left, right: right}, _) do
    left_door = inspect(left)
    right_door = inspect(right)

    left_data = inspect(Enum.reverse(Portal.Door.get(left)))
    right_data = inspect(Portal.Door.get(right))

    max = max(String.length(left_door), String.length(left_data))
    # changing the format of the sigil's items will mess-up the doctests!
    ~s(#Portal< #{left_door} <=> #{right_door} #{String.pad_leading(left_data, max)} <=> #{
      right_data
    }>)
  end
end
