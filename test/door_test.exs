defmodule DoorTest do
  use ExUnit.Case, async: false
  doctest Portal.Door

  # start a :red and a :blue door
  # for each test, then stop them afterward.
  # :red will always be left, and :blue right.
  setup do
    Portal.Door.start_link(:red)
    Portal.Door.start_link(:blue)
    :ok
  end

  test "new doors are initially empty" do
    assert Portal.Door.get(:red) == []
    assert Portal.Door.get(:blue) == []
  end

  test "a value into a door should be `get-able`" do
    Portal.Door.push(:red, 1)
    Portal.Door.push(:blue, 2)
    assert Portal.Door.get(:red) == [1]
    assert Portal.Door.get(:blue) == [2]
  end

  test "poping a door should return its head value" do
    Portal.Door.push(:red, 1)
    Portal.Door.push(:blue, 2)
    assert Portal.Door.pop(:red) == {:ok, 1}
    assert Portal.Door.pop(:blue) == {:ok, 2}
  end

  test "poping an empty returns :error" do
    assert Portal.Door.pop(:red) == :error
    assert Portal.Door.pop(:blue) == :error
  end

  test "a reset door should be empty" do
    Portal.Door.push(:red, 1)
    Portal.Door.push(:blue, 2)
    Portal.Door.reset(:red)
    Portal.Door.reset(:blue)
    assert Portal.Door.get(:red) == []
    assert Portal.Door.get(:blue) == []
  end
end
