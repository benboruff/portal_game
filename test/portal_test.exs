defmodule PortalTest do
  use ExUnit.Case, async: false
  doctest Portal

  setup do
    Portal.shoot(:orange)
    Portal.shoot(:green)
    portal = Portal.transfer(:orange, :green, [1, 2, 3])

    on_exit(fn ->
      Portal.Door.reset(:orange)
      Portal.Door.reset(:green)
    end)

    {:ok, [portal: portal]}
  end

  test "a transferred door should have left and right door colors to match", context do
    portal = context[:portal]
    assert Map.get(portal, :left) == :orange
    assert Map.get(portal, :right) == :green
  end

  test "a tranferred door should have correct initial values" do
    assert Enum.reverse(Portal.Door.get(:orange)) == [1, 2, 3]
    assert Portal.Door.get(:green) == []
  end

  test "push right should move item from left to right", context do
    portal = context[:portal]
    Portal.push_right(portal)
    assert Portal.Door.get(:green) == [3]
    assert Enum.reverse(Portal.Door.get(:orange)) == [1, 2]
  end

  test "push left should move item from right to left", context do
    portal = context[:portal]
    Portal.push_right(portal)
    Portal.push_left(portal)
    assert Portal.Door.get(:green) == []
    assert Enum.reverse(Portal.Door.get(:orange)) == [1, 2, 3]
  end
end
