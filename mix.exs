defmodule Portal.MixProject do
  use Mix.Project

  def project do
    [
      app: :portal_game,
      version: "0.1.0",
      elixir: "~> 1.9",
      description: description(),
      package: package(),
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "Portal Game",
      source_url: "https://gitlab.com/benboruff/portal_game",
      docs: [
        extras: ["README.md"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Portal.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.19", only: :dev, runtime: false}
    ]
  end

  defp description do
    "A simple package based on José Valim's Portal elixir demo."
  end

  defp package do
    [
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/benboruff/portal_game"}
    ]
  end
end
